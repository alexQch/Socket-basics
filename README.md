#README
This project provides a simplet chatting service where user can input their username and chat room name and chat together. 

The highlights of this prject includes:

* using `socket.io` for real time chatting experience
* using `jquery` for simple `DOM` manipulation
* using `moment.js` for converting global time to local time
* using `bootstrap.css` for simple styling

The finished UI is shown below:
Login page: 
![Selection_031.png](https://bitbucket.org/repo/rBBno9/images/1170029369-Selection_031.png)
Chatting Page: 
![Selection_030.png](https://bitbucket.org/repo/rBBno9/images/1725283754-Selection_030.png)
