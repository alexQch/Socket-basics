var moment = require('moment');

//get the moment object
var now = moment();

console.log(now.format());
console.log(now.format('X'));
console.log(now.format('x'));

now.subtract(1, 'year');

var timestamp = 1470300007;
//convert timestamp to moment instance
var timestampMoment = moment.utc(timestamp);
console.log(timestampMoment.local().format('h:mm a')); //11:06 am

console.log(timestampMoment.local().format('x')); //11:06 am

console.log(now.format());
//check the formating here: http://momentjs.com/docs/#/displaying/format/
console.log(now.format('MMM Do YYYY, h:mm a'));
