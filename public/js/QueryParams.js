//this function is for easily obtainning query parameters from the request URL
function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0, len = vars.length; i < len; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable ) {
            return decodeURIComponent(pair[1].replace(/\+/g, ' '));
        }
    }

    return undefined;
}
