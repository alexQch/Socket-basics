var name = getQueryVariable('name') || 'Anonymous';
var room = getQueryVariable('room');
var socket = io();

console.log(`${name} join ${room}`);

//modify the content of the room tag use jQuery
var $roomName = jQuery('.room-title');
$roomName.text(room);

//this callback fires everytime a client is connected
socket.on('connect', function(){
    console.log("Connected to socket.io server");
    socket.emit('joinRoom', {
        name: name,
        room: room
    });
});

socket.on('message', (message)=>{
    //a utc value of the time in milliseconds
    var momentTimeStamp = moment.utc(message.timestamp);
    var $messages = jQuery('.messages');
    var $message = jQuery('<li class="list-group-item"></li>');


    console.log("New message: ");
    console.log(message.text);

    //use jQuery to select eleements with class message
    //we use `append` to add new html contnet at the END of the element
    $message.append(`<p><strong>${message.name} ${momentTimeStamp.local().format('h:mm a')}</strong></p>`);
    $message.append('<p>' + message.text + '</p>');
    $messages.append($message);
})

//handle submitting of new message
//the $ sign indicates this var is created from jQuery
var $form = jQuery("#message-form");

$form.on("submit", (event)=>{

    //preventDefault is used on form when you don't want to submit in
    // old-fashioned way by refreshing the entire webpage especially when
    // using socket or making AJAX request, we want the form been
    // submitted as we want
    event.preventDefault();

    //find can search inside of an element
    //here we select all input elements with
    //an attribute name equals to message
    var $message = $form.find('input[name=message]');

    socket.emit('message', {
        name: name,
        text: $message.val()
        //the val here can pull the message
        //out and return an string
    });

    //erase the content of the message
    $message.val('');
    $message.focus();
});
